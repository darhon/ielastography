+++
title = "Прейскурант цін (клініка)"
id = ""
+++
---
## Прейскурант цін
### Медичного центру ТОВ «Інститут еластографії». Клініка.

<table border="1" cellspacing="0" cellpadding="5" align="middle">
<tbody>
<tr>
<th class="column-1"><b>Найменування послуги  </b></th>
<th class="column-1"><b>Ціна, з видачею протоколу (грн)</b></th>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Консультація лікаря спеціаліста (ендокринолог), первинна консультація </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Консультація лікаря спеціаліста (ендокринолог), повторна консультація </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Консультація лікаря спеціаліста (гастроентеролог) </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
</tbody>
</table>
