+++
title = "Прейскурант цен (диагностика)"
id = ""
+++
---
<h1 style="color:#ff0000">НЕТ РУССКОЙ ВЕРСИИ СТРАНИЦЫ</h1>
## Прейскурант цін
### Медичного центру ТОВ «Інститут еластографії». Діагностика.

<table border="1" cellspacing="0" cellpadding="5" align="middle">
<tbody>
<tr>
<th class="column-1"><b>Найменування послуги</b></th>
<th class="column-1"><b>Ціна, з видачею протоколу (грн)</b></th>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Дослідження ультразвукове комплексу інтима-медіа (КІМ) сонної артерії </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Транскраніальна доплерографія (голови)</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Дуплексне УЗ обстеження брахіоцефальних судин (шиї)</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Дуплексне УЗ обстеження брахіоцефальних судин та транскраніальна доплерографія </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Дуплексне УЗ обстеження (доплер) артерій або вен кінцівок (нижнії або верхніх) </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Дуплексне УЗ обстеження (доплер) вен та артерій кінцівок (нижнії або верхніх) </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Дуплексне УЗ обстеження (доплер) судин нирок    </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД. Черевна порожнина: печінки+жовчний міхур+підшлункова+селезінка  </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД. Черевна порожнина і нирки</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Черевна порожнина + еластографія (ступінь фіброзу) </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Черевна порожнина + еластографія (ступінь фіброзу) і нирки </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Черевна порожнина з доплерографією печінки + еластографія (ступінь фіброзу) </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Черевна порожнина+ еластографія та доплерографія печінки ,нирки </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Черевна порожнина з доплерографіэю судин </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД. Жовчний міхур (УЗ огляд)</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД. Передміхурова залоза (трансабдомінально) + сечовий міхур</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Передміхурова залоза (трансректально, ТРУЗД) + сечовий міхур</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД. Сечовий міхур з визначенням залишкової сечі </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД. Нирки+надниркові залози</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД. Матка+яєчники (трансабдомінально)+сечовий міхур </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Матка+яєчники (трансвагінально)+сечовий міхур</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Калитка </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Лімфовузли (одна анатомічна зона)</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Молочні залози (US-BIRADS)</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД. М'які тканини (одна анатомічна зона)</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД. Плевральна порожнина  </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">Мп-УЗД. Щитоподібна залоза (US-ThiRADS)  </span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД. Слинні залози</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
<tr>
<td class="column-1"><span style="font-weight: 400;">УЗД .Функція жовчного міхура</span></td>
<td class="column-1"><span style="font-weight: 400;"></span></td>
</tr>
</tbody>
</table>

* Мп-УЗД - мультипараметрична ультразвукова діагностика
