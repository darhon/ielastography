+++
title = "Марунчин Наталья Андреевна"
id = "marunchin"
+++
Марунчин Наталья Андреевна – врач-эндокринолог, член Европейской ассоциации по изучению патологии, лечения и регенерации печени (EASL), постоянно повышает квалификацию и проходит современное он-лайн обучение Medscape Diabetes and Endocrinology с получением соответственных сертификатов Европейского образца. Марунчин Наталья Андреевна имеет два высших образования – юридическое и медицинское. В клинической практике руководствуется протоколами МОЗ Украины, доказательной медициной с учетом индивидуального подхода к пациентам. В 2016 году прошла тематическое усовершенствование в Институте последипломного образования Национального медицинского университета имени А.А. Богомольца "Организация научных исследований: основы методологии и биостатистики", принимала участие в Workshop Каролинского института и НМУ имени А.А. Богомольца с епидемиологии. В 2018 году защитила диссертацию на тему: 'Оценка эффективности применения аморфного наноразмерного кремнезема у больных сахарным диабетом 2 типа с неалкогольной жировой болезнью печени'. Научное направление – неалкогольная жировая болезнь печени.

Врач-эндокринолог предоставляет консультации на украинском, русском, английском языках пациентам со следующими патологиями:

Сахарный диабет 1 типа;
Сахарный диабет 2 типа;
Заболевания щитовидной железы, гипоталамо-гипофизарной системы, надпочечников;
Нарушения кальциево-фосфорного, липидного обмена;
Метаболический синдром;
Лишний вес и ожирение;
Неалкогольная жировая болезнь печени.
Также врач-эндокринолог предоставляет рекомендации по модификации стиля жизни, выполняет индивидуальный подбор питания.

Первичная консультация врача-эндокринолога (осмотр, сбор анамнеза, назначение комплекса необходимых обследований, разъяснение результатов лабораторных и инструментальных исследований, предоставление рекомендаций, коррекция/назначение лечения): 580 грн.

Повторная консультация врача-эндокринолога (осмотр, назначение комплекса необходимых обследований, разъяснение результатов лабораторных и инструментальных исследований, предоставление рекомендаций, коррекция/назначение лечения): 330 грн.

[Сертификат](/pdf/EASL_certificate.pdf)

![](/img/certificates/info@printeximage1.jpg)

![](/img/certificates/info@printeximage2.jpg)

![](/img/certificates/info@printeximage3.jpg)

![](/img/certificates/info@printeximage4.jpg)

![](/img/certificates/info@printeximage5.jpg)

![](/img/certificates/info@printeximage6.jpg)

![](/img/certificates/info@printeximage7.jpg)

![](/img/certificates/info@printeximage8.jpg)

![](/img/certificates/info@printeximage9.jpg)

![](/img/certificates/info@printeximage10.jpg)

![](/img/certificates/info@printeximage11.jpg)
