+++
title = "Наші партнери"
id = "partners"
+++

 [![Кабінет лікаря-хірурга Бабія Олександра Михайловича](/institute/img/babiy.png)](http://medinfo.dp.ua/_firm.php?id=32)

 [![Медичний центр Doctor Vera](/institute/img/doctor_vera_1.png)](http://doctorvera.kiev.ua/)

 [![ДУ Інститут урології](/institute/img/institute_of_urology.jpg)](http://www.inurol.kiev.ua/)

 [![Клініка судинної патології](/institute/img/vascular-pathology-clinic.png)](https://lirnyk.com.ua/)

 [![Європейська клініка лікування печінки](/institute/img/regeneraciya.png)](http://regeneraciya.com.ua/)
